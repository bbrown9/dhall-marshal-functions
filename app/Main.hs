{- TODO
      * user-defined types for inputs
      * assert statements in dhall converted to predicates (or whatever is most
      appropriate) in haskell, which the idea being that we can convert tests
      in dhall to tests in haskell
    -}


{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE OverloadedRecordDot #-}
module Main where

import           Data.Functor.Contravariant (Predicate (..))
import           Dhall
import           GHC.Generics               (Generic)

-- Version with Predicate wrapper, which already has FromDhall instance
data AgePredicates
  = AgePredicates
      { isAdult      :: Predicate Natural
      , isNotRetiree :: Predicate Natural
      }
  deriving (FromDhall, Generic)

-- Dhall is record-field-name-aware
-- using  (ToDhall a, FromDhall b) => FromDhall (a -> b)
-- https://hackage.haskell.org/package/dhall-1.41.2/docs/Dhall-Marshal-Decode.html
data AgePredicatesBare
  = AgePredicatesBare
      { isAdultBare      :: Natural -> Bool
      , isNotRetireeBare :: Natural -> Bool
      }
  deriving (FromDhall, Generic)

-- Version with PredicateEval option
--

data PredicateEval = All | Any deriving (Eq, FromDhall, Generic, Show)

data AgePredicatesList
  = AgePredicatesList
      { predicates     :: [Predicate Natural]
      , predicatesEval :: PredicateEval
      }
  deriving (FromDhall, Generic)

evalAgePredicates :: AgePredicatesList -> Natural -> Bool
evalAgePredicates ps n = case predicatesEval ps of
                           -- This one is smooth
                           All -> getPredicate (mconcat $ predicates ps) n
                           -- This one shows Predicate monoid conveniences ^^
                           -- not worth it I think
                           Any -> any (\p -> getPredicate p n) $ predicates ps

-- Taking a as ToDhall d, FromDhall e => d -> e, should be able to derive
-- FromDhall for this
newtype AgePredicatesMulti
  = AgePredicatesMulti { isAgeGe :: Natural -> Natural -> Bool }
  deriving (FromDhall, Generic)

readAgePredicates:: IO AgePredicates
readAgePredicates = inputFile auto "./dhall/predicate.dhall"

readAgePredicatesBare :: IO AgePredicatesBare
readAgePredicatesBare = inputFile auto "./dhall/predicateBare.dhall"

readAgePredicatesList :: IO AgePredicatesList
readAgePredicatesList = inputFile auto "./dhall/predicateList.dhall"

readAgePredicatesMulti :: IO AgePredicatesMulti
readAgePredicatesMulti = inputFile auto "./dhall/predicateMulti.dhall"

main :: IO ()
main = do
  p <- readAgePredicates
  pB <- readAgePredicatesBare
  pL <- readAgePredicatesList
  pM <- readAgePredicatesMulti

  let x = 20
  let x' = 70
  putStrLn $ "isAdult " ++ show x
  print $ getPredicate (isAdult p) x

  putStrLn $ "isNotRetiree " ++ show x'
  print $ getPredicate (isNotRetiree p) x'
  putStrLn ""

  putStrLn $ "isAdult && isNotRetiree " ++ show x
  print $ getPredicate (isAdult p <> isNotRetiree p) x

  putStrLn $ "isAdult && isNotRetiree " ++ show x'
  print $ getPredicate (isAdult p <> isNotRetiree p) x'
  putStrLn ""

  putStrLn $ "isAdultBare && isNotRetireeBare " ++ show x
  print $ pB.isAdultBare x' && pB.isNotRetireeBare x

  putStrLn $ "isAdultBare && isNotRetireeBare " ++ show x'
  print $ pB.isAdultBare x' && pB.isNotRetireeBare x'
  putStrLn ""

  putStrLn $ "evalAgePredicates pL " ++ show x
  putStrLn $ "with pL.predicatesEval: " ++ show pL.predicatesEval
  print $ evalAgePredicates pL x

  putStrLn $ "evalAgePredicates pL " ++ show x'
  putStrLn $ "with pL.predicatesEval: " ++ show pL.predicatesEval
  print $ evalAgePredicates pL x'
  putStrLn ""

  putStrLn $ "isAgeGe " ++ show x ++ " " ++ show x'
  print $ pM.isAgeGe x x'
  putStrLn ""
