let PredicateEval = (./Type.dhall).PredicateEval

let preds = ./predicate.dhall

in  { predicatesEval = PredicateEval.Any
    , predicates = [ preds.isNotRetiree, preds.isAdult ]
    }
