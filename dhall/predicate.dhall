let p = ../Prelude.dhall

let Natural/gt = p.Natural.greaterThan

let Natural/lte = p.Natural.lessThanEqual

let isAdult = λ(age : Natural) → Natural/gt age 18

let isNotRetiree = λ(age : Natural) → Natural/lte age 65

in  { isNotRetiree, isAdult }
