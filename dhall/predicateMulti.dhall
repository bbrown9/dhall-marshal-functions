let p = ../Prelude.dhall

let Natural/gt = p.Natural.greaterThan

let isAgeGe = Natural/gt

in  { isAgeGe }
